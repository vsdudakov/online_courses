from django import forms
from django.utils.translation import ugettext_lazy as _ul
from django.contrib.auth import authenticate

from core.models import User, Specialization, Student, Teacher, ClassRoom
from core.helpers import form_fields_to_bootstrap


class ObjectForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(ObjectForm, self).__init__(*args, **kwargs)
        form_fields_to_bootstrap(self.fields) 

    class Meta:
        exclude = [
            'creation_date', 
            'modification_date',
            'is_active'
        ]


class FilterForm(forms.Form):

    def __init__(self, *args, **kwargs):
        super(FilterForm, self).__init__(*args, **kwargs)
        form_fields_to_bootstrap(self.fields) 


class LoginForm(forms.Form):
    username = forms.EmailField(label=_ul('Username'))
    password = forms.CharField(widget=forms.PasswordInput(), label=_ul('Password'))

    def clean(self):
        self.cleaned_data = super(LoginForm, self).clean()
        if 'password' in self.cleaned_data and 'username' in self.cleaned_data:
            self.cleaned_data['user'] = authenticate(
                username=self.cleaned_data['username'], 
                password=self.cleaned_data['password']
            )
            if not self.cleaned_data['user']:
                raise forms.ValidationError(_ul('Invalid username or password'))
        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        form_fields_to_bootstrap(self.fields) 


class UserForm(ObjectForm):

    class Meta(ObjectForm.Meta):
        model = User


class SpecializationForm(ObjectForm):

    class Meta(ObjectForm.Meta):
        model = Specialization


class UserObjectForm(ObjectForm):

    class Meta(ObjectForm.Meta):
        exclude = [
            'user',
        ] + ObjectForm.Meta.exclude    


class StudentForm(UserObjectForm):

    class Meta(UserObjectForm.Meta):
        model = Student


class UserFilterForm(FilterForm):
    first_name__icontains = forms.CharField(required=False, label=_ul('In first name'))
    last_name__icontains = forms.CharField(required=False, label=_ul('In last name'))
    user__username__icontains = forms.CharField(required=False, label=_ul('In username'))


class StudentFilterForm(UserFilterForm):
    classrooms__in = forms.ModelMultipleChoiceField(
        required=False, 
        label=_ul('Classrooms'), 
        queryset=ClassRoom.objects.filter(is_active=True)
    )


class TeacherForm(UserObjectForm):

    def __init__(self, *args, **kwargs):
        super(TeacherForm, self).__init__(*args, **kwargs)
        self.fields['specializations'].queryset = self.fields['specializations'].queryset.filter(
            is_active=True
        )

    class Meta(UserObjectForm.Meta):
        model = Teacher


class TeacherFilterForm(UserFilterForm):
    specializations__in = forms.ModelMultipleChoiceField(
        required=False, 
        label=_ul('Specializations'),
        queryset=Specialization.objects.filter(is_active=True)
    )


class ClassRoomForm(ObjectForm):

    def __init__(self, *args, **kwargs):
        super(ClassRoomForm, self).__init__(*args, **kwargs)
        self.fields['specialization'].queryset = self.fields['specialization'].queryset.filter(
            is_active=True
        )
        self.fields['teacher'].queryset = self.fields['teacher'].queryset.filter(
            is_active=True, user__is_active=True
        )
        self.fields['students'].queryset = self.fields['students'].queryset.filter(
            is_active=True, user__is_active=True
        )

    class Meta(ObjectForm.Meta):
        model = ClassRoom


class ClassRoomFilterForm(FilterForm):
    name__icontains = forms.CharField(required=False, label=_ul('In name'))
    description__icontains = forms.CharField(required=False, label=_ul('In description'))
    specialization__in = forms.ModelMultipleChoiceField(
        required=False, 
        label=_ul('Specializations'),
        queryset=Specialization.objects.filter(is_active=True)
    )
    teacher__in = forms.ModelMultipleChoiceField(
        required=False, 
        label=_ul('Teachers'),
        queryset=Teacher.objects.filter(is_active=True, user__is_active=True)
    )
    students__in = forms.ModelMultipleChoiceField(
        required=False, 
        label=_ul('Students'),
        queryset=Student.objects.filter(is_active=True, user__is_active=True)
    )