from django.test import TestCase
from django.core.exceptions import ValidationError

from core.models import User, Teacher, Student, Specialization


class CoreTestCase(TestCase):

    def test_delete_models(self):
        # User inherits from ObjectModel with special delete logic
        user = User.objects.create(
            username='test@test.com',
            role=User.ROLE_USER,
            is_active=True
        )
        user.delete()
        self.assertTrue(User.objects.filter(username='test@test.com', is_active=False).exists())

    def test_clean_student(self):
        # Check clean method logic for model Student
        user = User.objects.create(
            username='test@test.com',
            role=User.ROLE_USER,
            is_active=True
        )
        try:
            Teacher.objects.create(
                user=user,
                first_name='Teacher',
                last_name='Teacher',
            )
            student = Student(
                user=user,
                first_name='Student',
                last_name='Student'
            )
            student.clean()
            self.assertTrue(False)
        except ValidationError:
            pass

    def test_clean_teacher(self):
        # Check clean method logic for model Teacher
        user = User.objects.create(
            username='test@test.com',
            role=User.ROLE_USER,
            is_active=True
        )
        try:
            Student.objects.create(
                user=user,
                first_name='Student',
                last_name='Student'
            )
            teacher = Teacher(
                user=user,
                first_name='Teacher',
                last_name='Teacher',
            )
            teacher.clean()
            self.assertTrue(False)
        except ValidationError:
            pass
