from django.utils.html import escape as base_escape


def unescape(st):
    """unescape

    Note:
        For JS enjections
    """ 
    return st.\
        replace("&amp;", "&").\
        replace("&lt;", "<").\
        replace("&gt;", ">").\
        replace("&#39;", "'").\
        replace("&quot;", '"').\
        replace("&#x2F;", "/")


def escape(st):
    """escape

    Note:
        For JS enjections
    """ 
    return base_escape(st)


def form_fields_to_bootstrap(fields):
    """setup bootstrap classes for form fields

    Note:
        Uses in forms
    """ 
    for key in fields:
        field = fields[key]
        if field is None:
            continue
        field.widget.attrs.update({'class': field.widget.attrs.get('class', '') + ' form-control'})
        if field.__class__.__name__ in (
                'DateTimeField', 
                'DateField',
                'ChoiceField', 
                'ModelChoiceField',
                'MultipleChoiceField', 
                'ModelMultipleChoiceField'
            ):
            field.widget.attrs.update({'style': field.widget.attrs.get('style', '') + ' width: 100%;'})
        if field.__class__.__name__ in (
                'DateTimeField', 
                'DateField',
            ):
            field.widget.attrs.update({'class': field.widget.attrs.get('class', '') + ' datepicker'})
        if field.__class__.__name__ in (
                'TypedChoiceField',
                'ChoiceField', 
                'ModelChoiceField',
                'MultipleChoiceField', 
                'ModelMultipleChoiceField'
            ):
            field.widget.attrs.update({'class': field.widget.attrs.get('class', '') + ' select2'})
        if field.label and field.required:
            field.label += ' *'
