from tastypie.models import ApiKey, create_api_key

from django.db import models
from django.utils.translation import ugettext_lazy as _ul
from django.core.exceptions import ValidationError
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager


class ObjectModel(models.Model):
    """The abstract base class for other models.

    Note:
        django doesn't create DB table for this model cause this is abstract
        creation_date set automatically after creating object.
        modification_date set automatically after updating object.
        is_active - don't delete objects physically from DB, just setup this flag. 
    """

    creation_date = models.DateTimeField(auto_now_add=True)
    modification_date = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    def delete(self, *args, **kwargs):
        """This is overloaded 'delete' method.

        Note:
            This is for realising delete logic related to is_active flag.
            Just setup the flag as False and save at deleting object.
        """
        if self.pk is None:
            return
        self.is_active = False
        return self.save()

    class Meta:
        abstract = True


class UserManager(BaseUserManager):
    
    def create_superuser(self, username, password, **extra_fields):
        """This is overloaded 'create_superuser' method.

        Note:
            Logic related to creating superuser at running 
            management command createsuperuser
        """
        extra_fields['role'] = User.ROLE_ADMIN
        user = self.model(
            username=username,
            **extra_fields
        )
        user.set_password(password)
        user.save()
        return user


class User(AbstractBaseUser, ObjectModel):
    """Customized django user for standard django auth logic.
    
    Note:
        password field from AbstractBaseUser is in hash for security
    """ 
    ROLE_ADMIN = 0
    ROLE_USER = 1
    ROLES = (
        (ROLE_ADMIN, _ul(u'Admin')),
        (ROLE_USER, _ul(u'User'))
    )
    USERNAME_FIELD = 'username'

    username = models.EmailField(unique=True)
    role = models.PositiveIntegerField(choices=ROLES, default=ROLE_USER)

    @property
    def is_staff(self):
        return self.role == self.ROLE_ADMIN

    @property
    def is_superuser(self):
        return self.role == self.ROLE_ADMIN

    def get_apikey(self):
        apikey = ApiKey.objects.filter(user=self).first()
        if apikey:
            return apikey.key

    def is_teacher(self):
        return Teacher.objects.filter(user=self).exists()

    def get_teacher_profile(self):
        return Teacher.objects.filter(user=self).first()

    def is_student(self):
        return Student.objects.filter(user=self).exists()

    def get_student_profile(self):
        return Student.objects.filter(user=self).first()

    def has_module_perms(self, *args, **kwargs):
        return True

    def has_perm(self, *args, **kwarg):
        return True

    def get_short_name(self):
        if self.is_teacher():
            return str(self.get_teacher_profile())
        if self.is_student():
            return str(self.get_student_profile())
        return self.username

    objects = UserManager()

    class Meta:
        verbose_name = _ul(u'User')
        verbose_name_plural = _ul(u'Users')


# Create apikey for eahc created User for API auth by key
models.signals.post_save.connect(create_api_key, sender=User)


class UserObjectModel(ObjectModel):
    """The abstract base class for user models.
    
    Note:
        django doesn't create DB table for this model cause this is abstract
        user - o2o relation with user for auth
    """ 
    user = models.OneToOneField(User)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def delete(self, *args, **kwargs):
        if self.pk is None:
            return
        self.user.delete()
        return super(UserObjectModel, self).delete(*args, **kwargs)
 
    class Meta:
        abstract = True 


class Student(UserObjectModel):
    """Student
    
    Note:
        none
    """ 

    def clean(self):
        """Clean method for validation

        Note:
            Validate user relation
        """ 
        if Teacher.objects.filter(user=self.user).exists():
            raise ValidationError({'user': _ul('Teacher with this user exists.')})

    class Meta:
        verbose_name = _ul(u'Student')
        verbose_name_plural = _ul(u'Students')


class Specialization(ObjectModel):
    """Avalable specializations

    Note:
        none
    """ 
    name = models.CharField(unique=True, max_length=255)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _ul(u'Specialization')
        verbose_name_plural = _ul(u'Specializations')


class Teacher(UserObjectModel):
    """Teacher
    
    Note:
        Teacher has to have at least one specialization
        So m2m with blank=False
    """
    specializations = models.ManyToManyField(Specialization)

    def clean(self):
        """Clean method for validation

        Note:
            Validate user relation
        """ 
        if Student.objects.filter(user=self.user).exists():
            raise ValidationError({'user': _ul('Student with this user exists.')})

    class Meta:
        verbose_name = _ul(u'Teacher')
        verbose_name_plural = _ul(u'Teachers')



class ClassRoom(ObjectModel):
    """ClassRoom
    
    Note:
        Each ClassRoom has to have owner-teacher
        Each ClassRoom has to have specialization
        Each ClassRoom has to have unique name
    """
    name = models.CharField(unique=True, max_length=255)
    description = models.TextField(null=True, blank=True)
    specialization = models.ForeignKey(Specialization)
    teacher = models.ForeignKey(Teacher)
    students = models.ManyToManyField(Student, blank=True, related_name='classrooms')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _ul(u'Class Room')
        verbose_name_plural = _ul(u'Class Rooms')
