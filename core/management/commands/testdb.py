#-*- encoding: utf-8
from django.core.management.base import BaseCommand, CommandError
from django.core.management import call_command

from core.models import User, Specialization, Student, Teacher, ClassRoom


class Command(BaseCommand):

    def _create_test_user(self, data, role=User.ROLE_USER):
        # use get_or_create instead bulk_create cause 
        # we need to run this management commande more than one time
        user, is_created = User.objects.get_or_create(
            username=data + '@test.com',
            defaults={
                'role': role
            }
        )
        if is_created:
            # set password as hash
            user.set_password(data)
            user.save()
        else:
            user.role = role
            user.save()
        return user

    def _create_test_specialization(self, index):
        data = 'specialization_{}'.format(index)
        s, _ = Specialization.objects.get_or_create(name=data)
        return s

    def _create_test_student(self, index):
        data = 'student_{}'.format(index)
        user = self._create_test_user(data)
        student, is_created = Student.objects.get_or_create(
            user=user,
            defaults={
                'first_name': data,
                'last_name': data
            }
        )
        if not is_created:
            student.first_name = data
            student.last_name = data
            student.save()
        return student

    def _create_test_teacher(self, index):
        data = 'teacher_{}'.format(index)
        user = self._create_test_user(data)
        teacher, is_created = Teacher.objects.get_or_create(
            user=user,
            defaults={
                'first_name': data,
                'last_name': data
            }
        )
        if not is_created:
            teacher.first_name = data
            teacher.last_name = data
            teacher.save()
        else:
            teacher.specializations.add(self._create_test_specialization(index))
        return teacher

    def _create_test_classroom(self, index):
        data = 'classroom_{}'.format(index)
        teacher = self._create_test_teacher(index)
        specialization = self._create_test_specialization(index)
        classroom, is_created = ClassRoom.objects.get_or_create(
            name=data,
            defaults={
                'description': data,
                'specialization': specialization,
                'teacher': teacher
            }
        )
        if not is_created:
            classroom.description = data
            classroom.specialization = specialization
            classroom.teacher = teacher
            classroom.save()
        else:
            classroom.students.add(self._create_test_student(index))

    def _create_admin(self):
        self._create_test_user('admin', role=User.ROLE_ADMIN)

    def handle(self, *args, **kwargs):
        call_command('migrate', interactive=False)
        call_command('collectstatic', interactive=False)
        self._create_admin()

        # python3 works as xrange 
        for i in range(25):
            self._create_test_student(i)
            self._create_test_teacher(i)
            self._create_test_classroom(i)



