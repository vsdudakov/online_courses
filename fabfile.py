#-*- encoding: utf-8 -*-
import fabric
import os

SITES_PATH = os.path.join('/home', 'www-data', 'sites')
VIRTUALS_PATH = os.path.join('/home', 'www-data', 'virtuals') 

fabric.api.env.roledefs['production'] = ['185.22.62.11']


def production_env():
    fabric.api.env.project_root = os.path.join(SITES_PATH, 'production', 'online_courses')
    fabric.api.env.shell = '/bin/bash -c'
    fabric.api.env.python = os.path.join(VIRTUALS_PATH, 'production', 'oc_v', 'bin', 'python')
    fabric.api.env.pip = os.path.join(VIRTUALS_PATH, 'production', 'oc_v', 'bin', 'pip')
    fabric.api.env.uwsgi = os.path.join(VIRTUALS_PATH, 'production', 'oc_v', 'bin', 'uwsgi')  
    fabric.api.env.always_use_pty = False


def deploy():
    with fabric.api.cd(fabric.api.env.project_root):
        fabric.api.run('git pull origin master')
        fabric.api.run('{pip} install -r requirements.txt'.format(pip=fabric.api.env.pip))
        fabric.api.run('{python} manage.py initenv'.format(python=fabric.api.env.python))
        fabric.api.run('{uwsgi} --reload wsgi.pid'.format(uwsgi=fabric.api.env.uwsgi))


@fabric.api.roles('production')
def prod():
    production_env()
    deploy()
