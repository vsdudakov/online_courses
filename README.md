# Install enviroment and run dev server:  
cd online_cources  
online_courses$ sudo apt install python3-dev  
online_courses$ virtualenv -p python3 env  
online_courses$ source env/bin/activate  
(env)online_courses$ pip install -r requirements.txt  
(env)online_courses$ cp online_courses/settings.py.dev online_courses/settings.py   
(env)online_courses$ python manage.py testdb  
(env)online_courses$ python manage.py runserver  

# Technologies  
python3 - programming language  
django1.10 - web framework  
django-tastypie - rest for django  
fabric - deployment  
mommy - dynamic generation test data  
swagger - rest api doc (forked and fixed for django1.10 by me)  
bootstrap - css framework  
backbone - js mvc framework  
underscore - js templates  
django unittest - unit tests  

# Requirements  
See online_courses/requirements.docx  

# Tests
## Run unit tests  
(env)online_courses$ python manage.py test  
## Tests
online_courses/api/tests/* - tests for REST API  
online_courses/core/tests.py - tests for models  

# Structure of project  
online_courses/api/v1/resources/ - Tastypie resources for REST  
online_courses/core/models.py - Django ORM DB schema  
online_courses/frontend/static/js/ - Backbone MVC data  
online_courses/frontend/templates/partials/ - Underscore templates for Backbone  
online_courses/frontend/views.py - Django views for Index/Login/Logout  
online_courses/online_courses/base_settings.py - Django settings  
online_courses/fabfile.py - a fabric script for deployment to production    
online_courses/wsgi.ini - UWSGI settings  

# Test data  
(env)online_courses$ python manage.py testdb  
generate test data (see /core/management/commands/testdb.py)  
test students:  
login - student_{number}@test.com  
pass - student_{number}  
test teacher:  
login - teacher_{number}@test.com  
pass - teacher_{number}  
test admin:  
login - admin@test.com  
pass - admin

# Production  
http://oc.obelektrike.ru  
Login at any test account (see test data section)  
## Deployment
(env)online_courses$ fab prod  

# API  
http://oc.obelektrike.ru/admin/ - django admin (for admin users)  
http://oc.obelektrike.ru/api/doc/ - swagger api doc  (firstly explore api_key  for auth - get from django admin -> ApiKey table)  

