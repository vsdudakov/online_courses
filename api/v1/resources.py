import json
from tastypie.resources import ModelResource,  ALL_WITH_RELATIONS, ALL
from tastypie.authentication import Authentication, BasicAuthentication, ApiKeyAuthentication
from tastypie import fields, http

from core.helpers import unescape, escape
from core.models import User, Specialization, Student, Teacher, ClassRoom

from api.v1.authorizations import UserAuthorization, StudentAuthorization, TeacherAuthorization, ClassroomAuthorization
from api.v1.validations import StudentValidation, TeacherValidation, ClassroomValidation


class ObjectResource(ModelResource):

    def dehydrate(self, bundle):
        """dehydrate is run at GET

        Note:
            escape str against JS enjections
        """ 
        for key in bundle.data:
            if isinstance(bundle.data[key], str):
                bundle.data[key] = escape(bundle.data[key])
        return bundle

    def hydrate(self, bundle):
        """hydrate is run at POST, PUT

        Note:
            unescape str against JS enjections
        """ 
        for key in bundle.data:
            if isinstance(bundle.data[key], str):
                bundle.data[key] = unescape(bundle.data[key])
        return bundle

    def build_filters(self, filters=None, **kwargs):
        """hack for filtering multiple selects

        Note:
            backbone-> jquery.ajax setups get parameters with multiple data as key[]
            So, remove [] from key
        """ 
        if filters is not None:
            new_filters = {}
            for key in filters.keys():
                if hasattr(filters, 'getlist'):
                    value = filters.getlist(key)
                    new_filters[key.replace('[]','')] = value[0] if len(value) == 1 else ','.join(value)
                else:
                    new_filters[key] = filters[key]
            filters = new_filters
        return super(ObjectResource, self).build_filters(filters=filters, **kwargs)

    def error_response(self, request, errors, response_class=None):
        if response_class is None:
            response_class = http.HttpBadRequest
        humanity_errors = {
            'status': response_class.status_code
        }

        resource_errors = errors.get(self.__class__.__name__, None)
        if resource_errors:
            humanity_errors['statusText'] = str(resource_errors)
        elif 'error_message' in errors:
            humanity_errors['statusText'] = errors['error_message']
        return response_class(content=json.dumps(humanity_errors), content_type='application/json')

    class Meta:
        list_allowed_methods = ['get', 'post']
        detail_allowed_methods = ['get', 'put', 'delete']
        excludes = ['creation_date', 'modification_date']
        authentication = ApiKeyAuthentication()
        always_return_data = True


class SpecializationResource(ObjectResource):

    class Meta(ObjectResource.Meta):
        resource_name = 'SpecializationResource'
        queryset = Specialization.objects.filter(is_active=True)
        filtering = {
            'name': ALL
        }


class UserResource(ObjectResource):

    class Meta(ObjectResource.Meta):
        resource_name = 'UserResource'
        queryset = User.objects.filter(is_active=True)
        filtering = {
            'username': ALL
        }
        authorization = UserAuthorization()


class UserObjectResource(ObjectResource):
    user = fields.OneToOneField(UserResource, attribute='user', full=True)


class StudentResource(UserObjectResource):
    classrooms = fields.ToManyField('api.v1.resources.ClassRoomResource', attribute='classrooms', readonly=True)

    def hydrate(self, bundle, *args, **kwargs):
        if bundle.obj.pk:
            classroom_pk = bundle.data.get('classroom')
            if classroom_pk:
                try:
                    cr = ClassRoom.objects.get(id=classroom_pk)
                    if bundle.obj.pk not in cr.students.all().values_list('pk', flat=True):
                        cr.students.add(bundle.obj.pk)
                    else:
                        cr.students.remove(bundle.obj.pk)
                    
                except ClassRoom.DoesNotExist:
                    pass
        return super(StudentResource, self).hydrate(bundle, *args, **kwargs)

    class Meta(UserObjectResource.Meta):
        resource_name = 'StudentResource'
        queryset = Student.objects.filter(is_active=True)
        validation = StudentValidation()
        authorization = StudentAuthorization()
        filtering = {
            'first_name': ALL,
            'last_name': ALL,
            'user': ALL_WITH_RELATIONS,
            'classrooms': ALL_WITH_RELATIONS
        }


class TeacherResource(UserObjectResource):
    specializations = fields.ManyToManyField(SpecializationResource, attribute='specializations', full=True)

    class Meta(UserObjectResource.Meta):
        resource_name = 'TeacherResource'
        queryset = Teacher.objects.filter(is_active=True)
        validation = TeacherValidation()
        authorization = TeacherAuthorization()
        filtering = {
            'first_name': ALL,
            'last_name': ALL,
            'user': ALL_WITH_RELATIONS,
            'specializations': ALL_WITH_RELATIONS
        }


class ClassRoomResource(ObjectResource):
    specialization = fields.ForeignKey(SpecializationResource, attribute='specialization', full=True)
    teacher = fields.ForeignKey(TeacherResource, attribute='teacher', full=True)
    students = fields.ManyToManyField(StudentResource, attribute='students', full=True, blank=True)

    class Meta(ObjectResource.Meta):
        resource_name = 'ClassRoomResource'
        queryset = ClassRoom.objects.filter(is_active=True)
        validation = ClassroomValidation()
        authorization = ClassroomAuthorization()
        filtering = {
            'name': ALL,
            'description': ALL,
            'specialization': ALL_WITH_RELATIONS,
            'teacher': ALL_WITH_RELATIONS,
            'students': ALL_WITH_RELATIONS,
        }
