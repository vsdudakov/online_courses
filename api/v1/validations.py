from tastypie.validation import Validation

from django.utils.translation import ugettext as _u

from core.models import User, Specialization, Student, Teacher, ClassRoom


class StudentValidation(Validation):
    pass


class TeacherValidation(Validation):
    
    def is_valid(self, bundle, request=None):
        if not bundle.data:
            return {'__all__': _u('Not data')}
        errors = {}
        if not bundle.data.get('specializations'):
            errors['specializations'] = [_u('Required field')]
        return errors


class ClassroomValidation(Validation):

    def is_valid(self, bundle, request=None):
        if not bundle.data:
            return {'__all__': _u('Not data')}
        errors = {}
        if not bundle.data.get('teacher'):
            errors['teacher'] = [_u('Required field')]
        if not bundle.data.get('specialization'):
            errors['specialization'] = [_u('Required field')]
        return errors
