from tastypie.authorization import Authorization

from core.models import User, Specialization, Student, Teacher, ClassRoom


class UserAuthorization(Authorization):

    def read_list(self, object_list, bundle):
        return object_list.all() 

    def read_detail(self, object_list, bundle):
        return True

    def create_detail(self, object_list, bundle):
        user = bundle.request.user
        return user.role == user.ROLE_ADMIN or bundle.obj == user

    def update_detail(self, object_list, bundle):
        return self.create_detail(object_list, bundle)

    def delete_detail(self, object_list, bundle):
        return self.create_detail(object_list, bundle)


class StudentAuthorization(UserAuthorization):

    def create_detail(self, object_list, bundle):
        user = bundle.request.user
        return user.role == user.ROLE_ADMIN or bundle.obj.user == user


class TeacherAuthorization(UserAuthorization):

    def create_detail(self, object_list, bundle):
        user = bundle.request.user
        return user.role == user.ROLE_ADMIN or bundle.obj.user == user


class ClassroomAuthorization(Authorization):

    def read_list(self, object_list, bundle):
        return object_list.all()

    def read_detail(self, object_list, bundle):
        return True

    def create_detail(self, object_list, bundle):
        user = bundle.request.user
        return not user.is_student() or user.role == user.ROLE_ADMIN

    def update_detail(self, object_list, bundle):
        return self.create_detail(object_list, bundle)

    def delete_detail(self, object_list, bundle):
        return self.create_detail(object_list, bundle)
