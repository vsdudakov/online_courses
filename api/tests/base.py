import base64
import json

from django.test import Client

from core.models import User


class ApiClient(Client):

    def __init__(self, *args, **kwargs):
        super(ApiClient, self).__init__(*args, **kwargs)
        self.username = None
        self.api_key = None
    
    def login(self, username):
        try:
            user = User.objects.get(username=username)
            self.username = username
            self.api_key = user.get_apikey()
            if self.api_key:
                return True
        except User.DoesNotExist:
            pass
        return False

    def logout(self):
        self.username = None
        self.api_key = None

    def get_api_key_header(self):
        if self.username and self.api_key:
            return {
                'HTTP_AUTHORIZATION': 'ApiKey %s:%s' % (
                    self.username,
                    self.api_key
                )
            }
        return {}

    def get(self, *args, **kwargs):
        kwargs.update(self.get_api_key_header())
        return super(ApiClient, self).get(*args, **kwargs)

    def post(self, *args, **kwargs):
        kwargs.update(self.get_api_key_header())
        return super(ApiClient, self).post(*args, **kwargs)

    def put(self, *args, **kwargs):
        kwargs.update(self.get_api_key_header())
        return super(ApiClient, self).put(*args, **kwargs)

    def delete(self, *args, **kwargs):
        kwargs.update(self.get_api_key_header())
        return super(ApiClient, self).delete(*args, **kwargs)
