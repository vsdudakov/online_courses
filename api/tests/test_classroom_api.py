import json
from model_mommy import mommy

from django.test import TestCase

from api.tests.base import ApiClient
from core.models import User, Student, Teacher, ClassRoom, Specialization


class ApiV1ClassroomTestCase(TestCase):

    def setUp(self):
        self.admin = User(
            username='admin@admin.com',
            role=User.ROLE_ADMIN
        )
        self.admin.set_password('admin')
        self.admin.save()

        self.user_student = User(
            username='student@user.com',
            role=User.ROLE_USER
        )
        self.user_student.set_password('user')
        self.user_student.save()
        self.student = mommy.make(Student, user=self.user_student)

        self.user_teacher = User(
            username='teacher@user.com',
            role=User.ROLE_USER
        )
        self.user_teacher.set_password('user')
        self.user_teacher.save()
        self.teacher = mommy.make(Teacher, user=self.user_teacher)

        self.classroom = mommy.make(ClassRoom, description="test")

        self.client = ApiClient()

    def test_get_list_not_authentication(self):
        self.client.logout()
        response = self.client.get(
            '/api/v1/ClassRoomResource/'
        )  
        self.assertEqual(401, response.status_code)

    def test_get_list_by_teacher(self):
        mommy.make(
            ClassRoom,
            is_active=True,
            _quantity=4
        )
        mommy.make(
            ClassRoom,
            is_active=False,
            _quantity=4
        )
        self.client.login(self.user_teacher.username)
        response = self.client.get(
            '/api/v1/ClassRoomResource/'
        )
        self.assertEqual(200, response.status_code)
        content = json.loads(response.content.decode('utf-8'))
        count_classrooms = ClassRoom.objects.filter(is_active=True).count()
        objects = content.get('objects', [])
        self.assertEqual(len(objects), count_classrooms)

    def test_get_list_by_student(self):
        mommy.make(
            ClassRoom,
            is_active=True,
            _quantity=4
        )
        mommy.make(
            ClassRoom,
            is_active=False,
            _quantity=4
        )
        self.client.login(self.user_student.username)
        response = self.client.get(
            '/api/v1/ClassRoomResource/'
        )
        self.assertEqual(200, response.status_code)
        content = json.loads(response.content.decode('utf-8'))
        count_classrooms = ClassRoom.objects.filter(is_active=True).count()
        objects = content.get('objects', [])
        self.assertEqual(len(objects), count_classrooms)

    def test_get_object_not_authentication(self):
        self.client.logout()
        response = self.client.get(
            '/api/v1/ClassRoomResource/%s/'% self.classroom.pk
        )  
        self.assertEqual(401, response.status_code)

    def test_get_object_by_teacher(self):
        self.client.login(self.user_teacher.username)
        response = self.client.get(
            '/api/v1/ClassRoomResource/%s/'% self.classroom.pk
        ) 
        self.assertEqual(200, response.status_code)
        obj = json.loads(response.content.decode('utf-8'))
        classroom = ClassRoom.objects.get(id=self.classroom.pk)
        self.assertEqual(obj['id'], classroom.id)
        self.assertEqual(obj['name'], classroom.name)
        self.assertEqual(obj['description'], classroom.description)

    def test_get_object_by_student(self):
        self.client.login(self.user_student.username)
        response = self.client.get(
            '/api/v1/ClassRoomResource/%s/'% self.classroom.pk
        ) 
        self.assertEqual(200, response.status_code)
        obj = json.loads(response.content.decode('utf-8'))
        classroom = ClassRoom.objects.get(id=self.classroom.pk)
        self.assertEqual(obj['id'], classroom.id)
        self.assertEqual(obj['name'], classroom.name)
        self.assertEqual(obj['description'], classroom.description)

    def test_get_object_by_admin(self):
        self.client.login(self.admin.username)
        response = self.client.get(
            '/api/v1/ClassRoomResource/%s/'% self.classroom.pk
        ) 
        self.assertEqual(200, response.status_code)
        obj = json.loads(response.content.decode('utf-8'))
        classroom = ClassRoom.objects.get(id=self.classroom.pk)
        self.assertEqual(obj['id'], classroom.id)
        self.assertEqual(obj['name'], classroom.name)
        self.assertEqual(obj['description'], classroom.description)

    def test_put_object_not_authentication(self):
        self.client.logout()
        classroom_data = {
            "name": self.classroom.name,
            "description": self.classroom.description,
            "teacher": '/api/v1/TeacherResource/%s/' % self.classroom.teacher.pk,
            "specialization": '/api/v1/SpecializationResource/%s/' % self.classroom.specialization.pk,
            "students":[
                '/api/v1/StudentResource/%s/' % mommy.make(Student).pk
            ]
        }
        response = self.client.put(
            '/api/v1/ClassRoomResource/%s/' % self.classroom.pk,
            data=json.dumps(classroom_data), 
            content_type="application/json"
        ) 
        self.assertEqual(401, response.status_code)

    def test_put_object_by_student(self):
        self.client.login(self.user_student.username)
        classroom_data = {
            "name": self.classroom.name,
            "description": self.classroom.description,
            "teacher": '/api/v1/TeacherResource/%s/' % self.classroom.teacher.pk,
            "specialization": '/api/v1/SpecializationResource/%s/' % self.classroom.specialization.pk,
            "students":[
                '/api/v1/StudentResource/%s/' % mommy.make(Student).pk
            ]
        }
        response = self.client.put(
            '/api/v1/ClassRoomResource/%s/' % self.classroom.pk,
            data=json.dumps(classroom_data), 
            content_type="application/json"
        ) 
        self.assertEqual(401, response.status_code)

    def test_put_object_by_teacher(self):
        self.client.login(self.user_teacher.username)
        classroom_data = {
            "name": self.classroom.name,
            "description": self.classroom.description,
            "teacher": '/api/v1/TeacherResource/%s/' % self.classroom.teacher.pk,
            "specialization": '/api/v1/SpecializationResource/%s/' % self.classroom.specialization.pk,
            "students":[
                '/api/v1/StudentResource/%s/' % mommy.make(Student).pk
            ]
        }
        response = self.client.put(
            '/api/v1/ClassRoomResource/%s/' % self.classroom.pk,
            data=json.dumps(classroom_data), 
            content_type="application/json"
        ) 
        self.assertEqual(200, response.status_code)
        obj = json.loads(response.content.decode('utf-8'))
        classroom = ClassRoom.objects.get(id=self.classroom.pk)
        self.assertEqual(obj['id'], classroom.id)
        self.assertEqual(obj['name'], classroom.name)
        self.assertEqual(obj['description'], classroom.description)

    def test_put_object_by_admin(self):
        self.client.login(self.admin.username)
        classroom_data = {
            "name": self.classroom.name,
            "description": self.classroom.description,
            "teacher": '/api/v1/TeacherResource/%s/' % self.classroom.teacher.pk,
            "specialization": '/api/v1/SpecializationResource/%s/' % self.classroom.specialization.pk,
            "students":[
                '/api/v1/StudentResource/%s/' % mommy.make(Student).pk
            ]
        }
        response = self.client.put(
            '/api/v1/ClassRoomResource/%s/' % self.classroom.pk,
            data=json.dumps(classroom_data), 
            content_type="application/json"
        ) 
        self.assertEqual(200, response.status_code)
        obj = json.loads(response.content.decode('utf-8'))
        classroom = ClassRoom.objects.get(id=self.classroom.pk)
        self.assertEqual(obj['id'], classroom.id)
        self.assertEqual(obj['name'], classroom.name)
        self.assertEqual(obj['description'], classroom.description)

    def test_post_object_not_authentication(self):
        self.client.logout()
        classroom_data = {
            "name": "test_classroom",
            "description": "test_classroom",
            "teacher": '/api/v1/TeacherResource/%s/' % mommy.make(Teacher).pk,
            "specialization": '/api/v1/SpecializationResource/%s/' % mommy.make(Specialization).pk,
            "students":[
                '/api/v1/StudentResource/%s/' % mommy.make(Student).pk
            ]
        }
        response = self.client.post(
            '/api/v1/ClassRoomResource/',
            data=json.dumps(classroom_data), 
            content_type="application/json"
        )
        self.assertEqual(401, response.status_code)

    def test_post_object_by_student(self):
        self.client.login(self.user_student.username)
        classroom_data = {
            "name": "test_classroom",
            "description": "test_classroom",
            "teacher": '/api/v1/TeacherResource/%s/' % mommy.make(Teacher).pk,
            "specialization": '/api/v1/SpecializationResource/%s/' % mommy.make(Specialization).pk,
            "students":[
                '/api/v1/StudentResource/%s/' % mommy.make(Student).pk
            ]
        }
        response = self.client.post(
            '/api/v1/ClassRoomResource/',
            data=json.dumps(classroom_data), 
            content_type="application/json"
        )
        self.assertEqual(401, response.status_code)

    def test_post_object_by_teacher(self):
        self.client.login(self.user_teacher.username)
        classroom_data = {
            "name": "test_classroom",
            "description": "test_classroom",
            "teacher": '/api/v1/TeacherResource/%s/' % mommy.make(Teacher).pk,
            "specialization": '/api/v1/SpecializationResource/%s/' % mommy.make(Specialization).pk,
            "students":[
                '/api/v1/StudentResource/%s/' % mommy.make(Student).pk
            ]
        }
        response = self.client.post(
            '/api/v1/ClassRoomResource/',
            data=json.dumps(classroom_data), 
            content_type="application/json"
        )
        self.assertEqual(201, response.status_code)
        obj = json.loads(response.content.decode('utf-8'))
        self.assertEqual(obj['name'], "test_classroom")
        self.assertEqual(obj['description'], "test_classroom")

    def test_post_object_by_admin(self):
        self.client.login(self.admin.username)
        classroom_data = {
            "name": "test_classroom",
            "description": "test_classroom",
            "teacher": '/api/v1/TeacherResource/%s/' % mommy.make(Teacher).pk,
            "specialization": '/api/v1/SpecializationResource/%s/' % mommy.make(Specialization).pk,
            "students":[
                '/api/v1/StudentResource/%s/' % mommy.make(Student).pk
            ]
        }
        response = self.client.post(
            '/api/v1/ClassRoomResource/',
            data=json.dumps(classroom_data), 
            content_type="application/json"
        )
        self.assertEqual(201, response.status_code)
        obj = json.loads(response.content.decode('utf-8'))
        self.assertEqual(obj['name'], "test_classroom")
        self.assertEqual(obj['description'], "test_classroom")

    def test_delete_object_not_authentication(self):
        self.client.logout()
        response = self.client.delete(
            '/api/v1/ClassRoomResource/%s/' % self.classroom.pk,
        )
        self.assertEqual(401, response.status_code)

    def test_delete_object_by_student(self):
        self.client.login(self.user_student.username)
        response = self.client.delete(
            '/api/v1/ClassRoomResource/%s/' % self.classroom.pk,
        )
        self.assertEqual(401, response.status_code)

    def test_delete_object_by_teacher(self):
        self.client.login(self.user_teacher.username)
        response = self.client.delete(
            '/api/v1/ClassRoomResource/%s/' % self.classroom.pk,
        )
        self.assertEqual(204, response.status_code)
        classroom = ClassRoom.objects.get(id=self.classroom.pk)
        self.assertFalse(classroom.is_active)

    def test_delete_object_by_admin(self):
        self.client.login(self.admin.username)
        response = self.client.delete(
            '/api/v1/ClassRoomResource/%s/' % self.classroom.pk,
        )
        self.assertEqual(204, response.status_code)
        classroom = ClassRoom.objects.get(id=self.classroom.pk)
        self.assertFalse(classroom.is_active)
