import json
from model_mommy import mommy

from django.test import TestCase

from api.tests.base import ApiClient
from core.models import User, Student, Teacher, ClassRoom


class ApiV1StudentTestCase(TestCase):

    def setUp(self):
        self.admin = User(
            username='admin@admin.com',
            role=User.ROLE_ADMIN
        )
        self.admin.set_password('admin')
        self.admin.save()

        self.user_student = User(
            username='student@user.com',
            role=User.ROLE_USER
        )
        self.user_student.set_password('user')
        self.user_student.save()
        self.student = mommy.make(Student, user=self.user_student)

        self.user_student_1 = User(
            username='student1@user.com',
            role=User.ROLE_USER
        )
        self.user_student_1.set_password('user')
        self.user_student_1.save()
        self.student_1 = mommy.make(Student, user=self.user_student_1)

        self.user_teacher = User(
            username='teacher@user.com',
            role=User.ROLE_USER
        )
        self.user_teacher.set_password('user')
        self.user_teacher.save()
        self.teacher = mommy.make(Teacher, user=self.user_teacher)

        self.client = ApiClient()

    def test_get_list_not_authentication(self):
        self.client.logout()
        response = self.client.get(
            '/api/v1/StudentResource/'
        )  
        self.assertEqual(401, response.status_code)

    def test_get_list(self):
        mommy.make(
            Student,
            is_active=True,
            _quantity=4
        )
        mommy.make(
            Student,
            is_active=False,
            _quantity=4
        )
        self.client.login(self.user_student.username)
        response = self.client.get(
            '/api/v1/StudentResource/'
        )
        self.assertEqual(200, response.status_code)
        content = json.loads(response.content.decode('utf-8'))
        count_students = Student.objects.filter(is_active=True).count()
        objects = content.get('objects', [])
        self.assertEqual(len(objects), count_students)

    def test_get_object_not_authentication(self):
        self.client.logout()
        response = self.client.get(
            '/api/v1/StudentResource/%s/'% self.student.pk
        )  
        self.assertEqual(401, response.status_code)

    def test_get_object(self):
        self.client.login(self.user_student.username)
        response = self.client.get(
            '/api/v1/StudentResource/%s/'% self.student.pk
        ) 
        self.assertEqual(200, response.status_code)
        obj = json.loads(response.content.decode('utf-8'))
        student = Student.objects.get(id=self.student.pk)
        self.assertEqual(student.user.id, obj['user']['id'])
        self.assertEqual(obj['id'], student.id)
        self.assertEqual(obj['first_name'], student.first_name)
        self.assertEqual(obj['last_name'], student.last_name)

    def test_get_object_by_other_student(self):
        self.client.login(self.user_student_1.username)
        response = self.client.get(
            '/api/v1/StudentResource/%s/'% self.student.pk
        ) 
        self.assertEqual(200, response.status_code)
        obj = json.loads(response.content.decode('utf-8'))
        student = Student.objects.get(id=self.student.pk)
        self.assertEqual(student.user.id, obj['user']['id'])
        self.assertEqual(obj['id'], student.id)
        self.assertEqual(obj['first_name'], student.first_name)
        self.assertEqual(obj['last_name'], student.last_name)

    def test_get_object_by_admin(self):
        self.client.login(self.admin.username)
        response = self.client.get(
            '/api/v1/StudentResource/%s/'% self.student.pk
        ) 
        self.assertEqual(200, response.status_code)
        obj = json.loads(response.content.decode('utf-8'))
        student = Student.objects.get(id=self.student.pk)
        self.assertEqual(student.user.id, obj['user']['id'])
        self.assertEqual(obj['id'], student.id)
        self.assertEqual(obj['first_name'], student.first_name)
        self.assertEqual(obj['last_name'], student.last_name)

    def test_get_object_by_teacher(self):
        self.client.login(self.user_teacher.username)
        response = self.client.get(
            '/api/v1/StudentResource/%s/'% self.student.pk
        ) 
        self.assertEqual(200, response.status_code)
        obj = json.loads(response.content.decode('utf-8'))
        student = Student.objects.get(id=self.student.pk)
        self.assertEqual(student.user.id, obj['user']['id'])
        self.assertEqual(obj['id'], student.id)
        self.assertEqual(obj['first_name'], student.first_name)
        self.assertEqual(obj['last_name'], student.last_name)

    def test_put_object_not_authentication(self):
        self.client.logout()
        student_data = {
            "first_name": self.student.first_name,
            "last_name": self.student.last_name,
            "user": '/api/v1/UserResource/%s/' % self.student.user.pk,
        }
        response = self.client.put(
            '/api/v1/StudentResource/%s/' % self.student.pk,
            data=json.dumps(student_data), 
            content_type="application/json"
        ) 
        self.assertEqual(401, response.status_code)

    def test_put_object(self):
        self.client.login(self.user_student.username)
        student_data = {
            "first_name": self.student.first_name,
            "last_name": self.student.last_name,
            "user": '/api/v1/UserResource/%s/' % self.student.user.pk,
        }
        response = self.client.put(
            '/api/v1/StudentResource/%s/' % self.student.pk,
            data=json.dumps(student_data), 
            content_type="application/json"
        )
        self.assertEqual(200, response.status_code)
        obj = json.loads(response.content.decode('utf-8'))
        student = Student.objects.get(id=self.student.pk)
        self.assertEqual(student.user.id, obj['user']['id'])
        self.assertEqual(obj['id'], student.id)
        self.assertEqual(obj['first_name'], student.first_name)
        self.assertEqual(obj['last_name'], student.last_name)

    def test_put_object_by_admin(self):
        self.client.login(self.admin.username)
        student_data = {
            "first_name": self.student.first_name,
            "last_name": self.student.last_name,
            "user": '/api/v1/UserResource/%s/' % self.student.user.pk,
        }
        response = self.client.put(
            '/api/v1/StudentResource/%s/' % self.student.pk,
            data=json.dumps(student_data), 
            content_type="application/json"
        ) 
        self.assertEqual(200, response.status_code)
        obj = json.loads(response.content.decode('utf-8'))
        student = Student.objects.get(id=self.student.pk)
        self.assertEqual(student.user.id, obj['user']['id'])
        self.assertEqual(obj['id'], student.id)
        self.assertEqual(obj['first_name'], student.first_name)
        self.assertEqual(obj['last_name'], student.last_name)

    def test_put_object_by_teacher(self):
        self.client.login(self.user_teacher.username)
        student_data = {
            "first_name": self.student.first_name,
            "last_name": self.student.last_name,
            "user": '/api/v1/UserResource/%s/' % self.student.user.pk,
        }
        response = self.client.put(
            '/api/v1/StudentResource/%s/' % self.student.pk,
            data=json.dumps(student_data), 
            content_type="application/json"
        ) 
        self.assertEqual(401, response.status_code)

    def test_put_object_by_other_student(self):
        self.client.login(self.user_student_1.username)
        student_data = {
            "first_name": self.student.first_name,
            "last_name": self.student.last_name,
            "user": '/api/v1/UserResource/%s/' % self.student.user.pk,
        }
        response = self.client.put(
            '/api/v1/StudentResource/%s/' % self.student.pk,
            data=json.dumps(student_data), 
            content_type="application/json"
        ) 
        self.assertEqual(401, response.status_code)

    def test_post_object_not_authentication(self):
        self.client.logout()
        user = User(
            username='test_user@user.com',
            role=User.ROLE_USER
        )
        user.set_password('test_user')
        user.save()
        student_data = {
            "first_name": 'test_user',
            "last_name": 'test_user',
            "user": '/api/v1/UserResource/%s/' % user.pk,
        }
        response = self.client.post(
            '/api/v1/StudentResource/',
            data=json.dumps(student_data), 
            content_type="application/json"
        ) 
        self.assertEqual(401, response.status_code)

    def test_post_object_by_student(self):
        self.client.login(self.user_student.username)
        user = User(
            username='test_user@user.com',
            role=User.ROLE_USER
        )
        user.set_password('test_user')
        user.save()
        student_data = {
            "first_name": 'test_user',
            "last_name": 'test_user',
            "user": '/api/v1/UserResource/%s/' % user.pk,
        }
        response = self.client.post(
            '/api/v1/StudentResource/',
            data=json.dumps(student_data), 
            content_type="application/json"
        ) 
        self.assertEqual(401, response.status_code)

    def test_post_object_by_admin(self):
        self.client.login(self.admin.username)
        user = User(
            username='test_user@user.com',
            role=User.ROLE_USER
        )
        user.set_password('test_user')
        user.save()
        student_data = {
            "first_name": 'test_user',
            "last_name": 'test_user',
            "user": '/api/v1/UserResource/%s/' % user.pk,
        }
        response = self.client.post(
            '/api/v1/StudentResource/',
            data=json.dumps(student_data), 
            content_type="application/json"
        ) 
        self.assertEqual(201, response.status_code)
        obj = json.loads(response.content.decode('utf-8'))
        self.assertEqual(user.id, obj['user']['id'])
        self.assertEqual(obj['first_name'], 'test_user')
        self.assertEqual(obj['last_name'], 'test_user')
    
    def test_post_object_by_teacher(self):
        self.client.login(self.user_teacher.username)
        user = User(
            username='test_user@user.com',
            role=User.ROLE_USER
        )
        user.set_password('test_user')
        user.save()
        student_data = {
            "first_name": 'test_user',
            "last_name": 'test_user',
            "user": '/api/v1/UserResource/%s/' % user.pk,
        }
        response = self.client.post(
            '/api/v1/StudentResource/',
            data=json.dumps(student_data), 
            content_type="application/json"
        ) 
        self.assertEqual(401, response.status_code)

    def test_delete_object_not_authentication(self):
        self.client.logout()
        response = self.client.delete(
            '/api/v1/StudentResource/%s/' % self.student.pk,
        )
        self.assertEqual(401, response.status_code)

    def test_delete_object_by_teacher(self):
        self.client.login(self.user_teacher.username)
        response = self.client.delete(
            '/api/v1/StudentResource/%s/' % self.student.pk,
        )
        self.assertEqual(401, response.status_code)

    def test_delete_object_by_other_student(self):
        self.client.login(self.user_student_1.username)
        response = self.client.delete(
            '/api/v1/StudentResource/%s/' % self.student.pk,
        )
        self.assertEqual(401, response.status_code)

    def test_delete_object(self):
        self.client.login(self.user_student.username)
        response = self.client.delete(
            '/api/v1/StudentResource/%s/' % self.student.pk,
        )
        self.assertEqual(204, response.status_code)
        student = Student.objects.get(id=self.student.pk)
        self.assertFalse(student.is_active)

    def test_delete_admin(self):
        self.client.login(self.admin.username)
        response = self.client.delete(
            '/api/v1/StudentResource/%s/' % self.student.pk,
        )
        self.assertEqual(204, response.status_code)
        student = Student.objects.get(id=self.student.pk)
        self.assertFalse(student.is_active)