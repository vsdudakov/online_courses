from tastypie.api import Api

from django.conf.urls import url, include
from django.contrib import admin

from api.v1 import resources

v1_api = Api(api_name='v1')
v1_api.register(resources.TeacherResource())
v1_api.register(resources.StudentResource())
v1_api.register(resources.ClassRoomResource())
v1_api.register(resources.UserResource())
v1_api.register(resources.SpecializationResource())


urlpatterns = [
    url(r'', include(v1_api.urls)),
]
