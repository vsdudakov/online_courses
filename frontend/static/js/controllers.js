'use strict';


var Controller = Backbone.Router.extend({
    routes: {
        "": "index",

        "classroom/list/:page/": "classRoomList",
        "classroom/create/": "classRoomCreate",
        "classroom/update/:id/": "classRoomUpdate",
        "classroom/detail/:id/": "classRoomDetail",
        "classroom/delete/:id/": "classRoomDelete",

        "teacher/list/:page/": "teacherList",
        "teacher/update/:id/": "teacherUpdate",
        "teacher/detail/:id/": "teacherDetail",
        "teacher/delete/:id/": "teacherDelete",

        "student/list/:page/": "studentList",
        "student/update/:id/": "studentUpdate",
        "student/detail/:id/": "studentDetail",
        "student/delete/:id/": "studentDelete",

        "*pageNotFound": "pageNotFound"
    },

    index: function () {
        this.navigate("classroom/list/1/", {trigger: true, replace: true});
    },

    classRoomList: function (page) {
        Views.objClassRoomListView.render(page);
    },

    classRoomCreate: function () {
        Views.objClassRoomCreateView.render();
    },

    classRoomUpdate: function (id) {
        Views.objClassRoomUpdateView.render(id);
    },

    classRoomDetail: function (id) {
        Views.objClassRoomDetailView.render(id);
    },

    classRoomDelete: function (id) {
        Views.objClassRoomDeleteView.render(id);
    },

    teacherList: function (page) {
        Views.objTeacherListView.render(page);
    },

    teacherUpdate: function (id) {
        Views.objTeacherUpdateView.render(id);
    },

    teacherDetail: function (id) {
        Views.objTeacherDetailView.render(id);
    },

    teacherDelete: function (id) {
        Views.objTeacherDeleteView.render(id);
    },

    studentList: function (page) {
        Views.objStudentListView.render(page);
    },

    studentUpdate: function (id) {
        Views.objStudentUpdateView.render(id);
    },

    studentDetail: function (id) {
        Views.objStudentDetailView.render(id);
    },

    studentDelete: function (id) {
        Views.objStudentDeleteView.render(id);
    },

    pageNotFound: function () {
        Views.objPageNotFoundView.render();
    }
});

var controller = new Controller(); 

Backbone.history.start();
