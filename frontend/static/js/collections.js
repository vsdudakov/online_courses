'use strict';


var BaseCollection = Backbone.Collection.extend({
});


var ClassRoomCollection = BaseCollection.extend({
    url: '/api/v1/ClassRoomResource/',
    model: ClassRoomModel
});


var TeacherCollection = BaseCollection.extend({
    url: '/api/v1/TeacherResource/',
    model: TeacherModel
});


var StudentCollection = BaseCollection.extend({
    url: '/api/v1/StudentResource/',
    model: StudentModel
});
