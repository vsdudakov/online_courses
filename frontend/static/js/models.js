'use strict';


var BaseModel = Backbone.Model.extend({
});


var ClassRoomModel = BaseModel.extend({
    urlRoot: '/api/v1/ClassRoomResource/',
    defaults: {
    },
    idAttribute: '_id'
});


var TeacherModel = BaseModel.extend({
    urlRoot: '/api/v1/TeacherResource/',
    defaults: {
    },
    idAttribute: '_id'
});


var StudentModel = BaseModel.extend({
    urlRoot: '/api/v1/StudentResource/',
    defaults: {
    },
    idAttribute: '_id'
});
