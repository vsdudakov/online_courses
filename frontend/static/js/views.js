'use strict';

var Views = { };

var notificationService = {};
_.extend(notificationService, Backbone.Events);



$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};


var SpinnerView = Backbone.View.extend({
    el: $("#spinner"),

    initialize : function () {
        this.listenTo(notificationService, 'showSpinner', this.show);
        this.listenTo(notificationService, 'hideSpinner', this.hide);
    },

    show: function () {
        this.$el.show();
    },

    hide: function () {
        this.$el.hide();
    } 
});


var MsgView = Backbone.View.extend({
    el: $("#msg"),
    template: _.template($('#msg-template').html()),

    initialize : function () {
        this.listenTo(notificationService, 'showMsg', this.show);
        this.listenTo(notificationService, 'hideMsg', this.hide);
    },

    show: function (msg) {
        this.$el.html(this.template({error: msg}));
    },

    hide: function () {
        this.$el.html('');
    }

});


var BaseView = Backbone.View.extend({
    el: $("#content"),

    render: function () {
        notificationService.trigger('renderNavBar');
        notificationService.trigger('hideMsg');
        this.$el.html(this.template());
    } 
}); 


var BaseListView = BaseView.extend({
    pagination: 10,
    filters: {},
    page: 1,
    order_by: null,

    fetchSuccess: function(collection, response, options) {
        notificationService.trigger('hideSpinner');
        var pages = parseInt(collection.meta.total_count / this.pagination);
        if (collection.meta.total_count % this.pagination > 0) {
            pages = pages + 1;
        }
        this.$el.html(this.template({objects: collection, page: this.page, pages: pages}));
    },

    fetchError: function(collection, response, options) {
        notificationService.trigger('hideSpinner');
        notificationService.trigger('showMsg', response);
    },

    filterSuccess: function() {
        this.filters = this.$el.find('form').serializeObject();
        for (var field in this.filters) {
            if (!this.filters[field]) {
                delete this.filters[field];
            }
        };
        this.render(this.page, this.filters);
        return false;
    },

    render: function (page, filters) {
        var filters = (typeof filters !== 'undefined') ?  filters : {};
        notificationService.trigger('renderNavBar');
        notificationService.trigger('hideMsg');
        notificationService.trigger('showSpinner');
        this.page = page;
        filters["limit"] = this.pagination;
        filters["offset"] = (parseInt(page) - 1) * this.pagination;
        var self = this;
        this.collection.fetch({
            data: filters,
            success: function(collection, response, options) {
                self.fetchSuccess(collection, response, options);
            },
            error: function(collection, response, options) {
                self.fetchError(collection, response, options);
            }
        }); 
    } 
});


var BaseDetailView = BaseView.extend({

    fetchSuccess: function(model, response, options) {
        notificationService.trigger('hideSpinner');
        this.$el.html(this.template({object: model}));
    },

    fetchError: function(model, response, options) {
        notificationService.trigger('hideSpinner');
        notificationService.trigger('showMsg', response);
    },

    render: function (id) {
        notificationService.trigger('renderNavBar');
        notificationService.trigger('hideMsg');
        notificationService.trigger('showSpinner');
        this.model.clear();
        this.model.set({id: id, _id: id});
        var self = this;
        this.model.fetch({
            success: function(model, response, options) {
                self.fetchSuccess(model, response, options);
            },
            error: function(model, response, options) {
                self.fetchError(model, response, options);
            }
        }); 
    }

});


var BaseCreateView = BaseView.extend({

    render: function () {
        this.model.clear();
        BaseView.prototype.render.call(this);
    },

    getSuccessUrl: function (model) {
    },

    saveSuccess: function(model, response, options) {
        notificationService.trigger('hideSpinner');
        Backbone.history.navigate(this.getSuccessUrl(model), true);
    },

    saveError: function(model, response, options) {
        notificationService.trigger('hideSpinner');
        notificationService.trigger('showMsg', response);
    },

    serializeForm: function() {
        return this.$el.find('form').serializeObject();
    },

    createData: function () {
        notificationService.trigger('showSpinner');
        this.model.clear();
        this.model.set(this.serializeForm());
        var self = this;
        this.model.save(null, {
            success: function(model, response, options){
                self.saveSuccess(model, response, options);
            },
            error: function(model, response, options){
                var json_response = null;
                try {
                    json_response = JSON.parse(response);
                } catch(e) {
                    json_response = response.responseJSON;
                }
                self.saveError(model, json_response, options);
            }            
        });
        return false;
    } 
}); 


var BaseUpdateView = BaseDetailView.extend({

    getSuccessUrl: function (model) {
    },

    saveSuccess: function(model, response, options) {
        notificationService.trigger('hideSpinner');
        Backbone.history.navigate(this.getSuccessUrl(model), true);
    },

    saveError: function(model, response, options) {
        notificationService.trigger('hideSpinner');
        notificationService.trigger('showMsg', response);
    },

    serializeForm: function() {
        return this.$el.find('form').serializeObject();
    },

    updateData: function() {
        this.model.set(this.serializeForm());
        notificationService.trigger('showSpinner');
        var self = this;
        this.model.save(null, {
            type: 'PUT',
            success: function(model, response, options){
                self.saveSuccess(model, response, options);
            },
            error: function(model, response, options){
                var json_response = null;
                try {
                    json_response = JSON.parse(response);
                } catch(e) {
                    json_response = response.responseJSON;
                }
                self.saveError(model, json_response, options);
            }  
        });
        return false;
    } 
}); 


var BaseDeleteView = Backbone.View.extend({

    getSuccessUrl: function (model) {
    },

    deleteSuccess: function(model, response, options) {
        notificationService.trigger('hideSpinner');
        Backbone.history.navigate(this.getSuccessUrl(model), true);
    },

    deleteError: function(model, response, options) {
        notificationService.trigger('hideSpinner');
        notificationService.trigger('showMsg', response);
    },

    render: function (id) {
        notificationService.trigger('renderNavBar');
        notificationService.trigger('hideMsg');
        notificationService.trigger('showSpinner');
        this.model.clear();
        this.model.set({id: id, _id: id});
        var self = this;
        this.model.destroy({
            success: function(model, response, options){
                self.deleteSuccess(model, response, options);
            },
            error: function(model, response, options){
                self.deleteError(model, response, options);
            }
        }); 
    } 
});


var TeacherListView = BaseListView.extend({
    collection: new TeacherCollection(),
    template: _.template($('#teacher-list-template').html()),

    events: {
        "click #id-btn-teacher-filter": "filterSuccess"
    },

    fetchSuccess: function(collection, response, options) {
        BaseListView.prototype.fetchSuccess.call(this, collection, response, options);
        this.$el.find("#id_username__icontains").val(this.filters["username__icontains"]);
        this.$el.find("#id_first_name__icontains").val(this.filters["first_name__icontains"]);
        this.$el.find("#id_last_name__icontains").val(this.filters["last_name__icontains"]);
        this.$el.find("#id_specializations__in").val(this.filters["specializations__in"]);
        this.$el.find(".select2").select2();
    }
});


var TeacherDetailView = BaseDetailView.extend({
    model: new TeacherModel(),
    template: _.template($('#teacher-detail-template').html())
});


var TeacherUpdateView = BaseUpdateView.extend({
    model: new TeacherModel(),
    template: _.template($('#teacher-update-template').html()),

    events: {
        "click #id-btn-teacher-update": "updateData"
    },

    getSuccessUrl: function (model) {
        return 'teacher/detail/' + model.get('id') + '/';
    },

    serializeForm: function() {
        var data = BaseUpdateView.prototype.serializeForm.call(this);
        if (!$.isArray(data.specializations)){
            data.specializations = [data.specializations]
        }
        data.specializations = data.specializations.filter(function(id) {
            return typeof(id) != "undefined";
        })
        data.specializations = data.specializations.map(function(id) {
            return '/api/v1/SpecializationResource/' + id + '/';
        })
        return data;
    },

    fetchSuccess: function(model, response, options) {
        BaseUpdateView.prototype.fetchSuccess.call(this, model, response, options);
        this.$el.find("form #id_" + 'first_name').val(model.get('first_name'));
        this.$el.find("form #id_" + 'last_name').val(model.get('last_name'));
        this.$el.find("form #id_" + 'specializations').val(model.get('specializations').map(function(s) {
            return s.id;
        }));
        this.$el.find(".select2").select2();
    }
});


var TeacherDeleteView = BaseDeleteView.extend({
    model: new TeacherModel(),

    deleteSuccess: function(model, response, options) {
        window.location.href = "/logout/";
    }
});


var StudentListView = BaseListView.extend({
    collection: new StudentCollection(),
    template: _.template($('#student-list-template').html()),

    events: {
        "click #id-btn-student-filter": "filterSuccess"
    },

    fetchSuccess: function(collection, response, options) {
        BaseListView.prototype.fetchSuccess.call(this, collection, response, options);
        this.$el.find("#id_username__icontains").val(this.filters["username__icontains"]);
        this.$el.find("#id_first_name__icontains").val(this.filters["first_name__icontains"]);
        this.$el.find("#id_last_name__icontains").val(this.filters["last_name__icontains"]);
        this.$el.find("#id_classrooms__in").val(this.filters["classrooms__in"]);
        this.$el.find(".select2").select2();
    }
});


var StudentDetailView = BaseDetailView.extend({
    model: new StudentModel(),
    template: _.template($('#student-detail-template').html())
});


var StudentUpdateView = BaseUpdateView.extend({
    model: new StudentModel(),
    template: _.template($('#student-update-template').html()),

    events: {
        "click #id-btn-student-update": "updateData"
    },

    getSuccessUrl: function (model) {
        return 'student/detail/' + model.get('id') + '/';
    },

    fetchSuccess: function(model, response, options) {
        BaseUpdateView.prototype.fetchSuccess.call(this, model, response, options);
        this.$el.find("form #id_" + 'first_name').val(model.get('first_name'));
        this.$el.find("form #id_" + 'last_name').val(model.get('last_name'));
        this.$el.find(".select2").select2();
    }
});


var StudentDeleteView = BaseDeleteView.extend({
    model: new StudentModel(),

    deleteSuccess: function(model, response, options) {
        window.location.href = "/logout/";
    }
});


var ClassRoomListView = BaseListView.extend({
    collection: new ClassRoomCollection(),
    template: _.template($('#classroom-list-template').html()),

    events: {
        "click #id-btn-classroom-filter": "filterSuccess"
    },

    fetchSuccess: function(collection, response, options) {
        BaseListView.prototype.fetchSuccess.call(this, collection, response, options);
        this.$el.find("#id_name__icontains").val(this.filters["name__icontains"]);
        this.$el.find("#id_description__icontains").val(this.filters["description__icontains"]);
        this.$el.find("#id_specialization__in").val(this.filters["specialization__in"]);
        this.$el.find("#id_teacher__in").val(this.filters["teacher__in"]);
        this.$el.find("#id_students__in").val(this.filters["students__in"]);
        this.$el.find(".select2").select2();
    }
});


var ClassRoomDetailView = BaseDetailView.extend({
    model: new ClassRoomModel(),
    template: _.template($('#classroom-detail-template').html()),

    events: {
        "click #id-btn-classroom-join": "joinAction"
    },

    joinAction: function() {
        var self = this;
        var model = new StudentModel({id: $('#id-btn-classroom-join').attr('data')});
        model.fetch({
            success: function(model, response, options) {
                model.set({classroom: self.model.get('id')})
                model.save(null, {
                    type: 'PUT',
                    success: function(model, response, options){
                        self.render();
                    }
                }); 
            }
        }); 
    }

});


var ClassRoomCreateView = BaseCreateView.extend({
    model: new ClassRoomModel(),
    template: _.template($('#classroom-create-template').html()),

    events: {
        "click #id-btn-classroom-create": "createData"
    },

    getSuccessUrl: function (model) {
        return 'classroom/detail/' + model.get('id') + '/';
    },

    serializeForm: function() {
        var data = BaseUpdateView.prototype.serializeForm.call(this);
        if (!$.isArray(data.students)){
            data.students = [data.students]
        }
        data.students = data.students.filter(function(id) {
            return typeof(id) != "undefined";
        })
        data.students = data.students.map(function(id) {
            return '/api/v1/StudentResource/' + id + '/';
        })

        if (data.specialization) {
            data.specialization = '/api/v1/SpecializationResource/' + data.specialization + '/';
        }
        if (data.teacher) {
            data.teacher = '/api/v1/TeacherResource/' + data.teacher + '/';
        }
        return data;
    },

    render: function () {
        BaseCreateView.prototype.render.call(this);
        this.$el.find(".select2").select2();
    }
});


var ClassRoomUpdateView = BaseUpdateView.extend({
    model: new ClassRoomModel(),
    template: _.template($('#classroom-update-template').html()),

    events: {
        "click #id-btn-classroom-update": "updateData"
    },

    getSuccessUrl: function (model) {
        return 'classroom/detail/' + model.get('id') + '/';
    },

    serializeForm: function() {
        var data = BaseUpdateView.prototype.serializeForm.call(this);
        if (!$.isArray(data.students)){
            data.students = [data.students]
        }
        data.students = data.students.filter(function(id) {
            return typeof(id) != "undefined";
        })
        data.students = data.students.map(function(id) {
            return '/api/v1/StudentResource/' + id + '/';
        })

        if (data.specialization) {
            data.specialization = '/api/v1/SpecializationResource/' + data.specialization + '/';
        }
        if (data.teacher) {
            data.teacher = '/api/v1/TeacherResource/' + data.teacher + '/';
        }
        return data;
    },

    fetchSuccess: function(model, response, options) {
        BaseUpdateView.prototype.fetchSuccess.call(this, model, response, options);
        this.$el.find("form #id_" + 'name').val(model.get('name'));
        this.$el.find("form #id_" + 'description').val(model.get('description'));
        this.$el.find("form #id_" + 'specialization').val(model.get('specialization').id);
        this.$el.find("form #id_" + 'teacher').val(model.get('teacher').id);
        this.$el.find("form #id_" + 'students').val(model.get('students').map(function(s) {
            return s.id;
        }));
        this.$el.find(".select2").select2();
    }
});


var ClassRoomDeleteView = BaseDeleteView.extend({
    model: new ClassRoomModel(),

    getSuccessUrl: function (model) {
        return 'classroom/list/1/';
    }
});


var PageNotFoundView = BaseView.extend({
    template: _.template($('#page-not-found-template').html())
});


Views = { 
    objSpinnerView: new SpinnerView(),
    objMsgView: new MsgView(),

    objClassRoomListView: new ClassRoomListView(),
    objClassRoomCreateView: new ClassRoomCreateView(),
    objClassRoomDetailView: new ClassRoomDetailView(),
    objClassRoomUpdateView: new ClassRoomUpdateView(),
    objClassRoomDeleteView: new ClassRoomDeleteView(),

    objTeacherListView: new TeacherListView(),
    objTeacherDetailView: new TeacherDetailView(),
    objTeacherUpdateView: new TeacherUpdateView(),
    objTeacherDeleteView: new TeacherDeleteView(),

    objStudentListView: new StudentListView(),
    objStudentDetailView: new StudentDetailView(),
    objStudentUpdateView: new StudentUpdateView(),
    objStudentDeleteView: new StudentDeleteView(),

    objPageNotFoundView: new PageNotFoundView()
}