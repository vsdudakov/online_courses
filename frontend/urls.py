from django.conf.urls import url

from frontend.views import IndexView, LoginView, LogoutView


urlpatterns = [ 
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
]
