from django.views.generic import TemplateView, RedirectView, FormView
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.urls import reverse_lazy
from django.contrib.auth import logout, login


from core.forms import (
    LoginForm,
    TeacherForm, 
    TeacherFilterForm, 
    StudentForm, 
    StudentFilterForm, 
    ClassRoomForm,
    ClassRoomFilterForm
)


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        kwargs = super(IndexView, self).get_context_data(**kwargs)
        kwargs['teacher_form'] = TeacherForm()
        kwargs['teacher_filter_form'] = TeacherFilterForm()
        kwargs['student_form'] = StudentForm()
        kwargs['student_filter_form'] = StudentFilterForm()
        kwargs['classroom_form'] = ClassRoomForm()
        kwargs['classroom_filter_form'] = ClassRoomFilterForm()
        return kwargs

    @method_decorator(login_required(login_url=reverse_lazy('login')))
    def dispatch(self, *args, **kwargs):
        return super(IndexView, self).dispatch(*args, **kwargs)


class LoginView(FormView):
    template_name = 'login.html'
    form_class = LoginForm

    def get_success_url(self):
        next_url = self.request.GET.get('next', None)
        if next_url is not None:
            return next_url
        return reverse_lazy('index')

    def form_valid(self, form):
        username = form.cleaned_data['username']
        user = form.cleaned_data['user']
        if user is not None:
            login(self.request, user)
        return super(LoginView, self).form_valid(form)


class LogoutView(RedirectView):
    url = reverse_lazy('login')

    def get_redirect_url(self):
        logout(self.request)
        return super(LogoutView, self).get_redirect_url()

